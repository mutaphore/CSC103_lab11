import java.util.*;

public class MyListDriver {

	public static void main(String[] args) {
		
		MySortedList list = new MySortedList();
		Scanner input = new Scanner(System.in);
		char choice = 'z';
		int temp;
		
		System.out.println("Choose one of the following operations:");
		System.out.println("- add (enter the letter a)");
                System.out.println("- delete (enter the letter d)");			          
		System.out.println("- max (enter the letter x)");
		System.out.println("- min (enter the letter m)");
		System.out.println("- print (enter the letter p)");
		System.out.println("- is empty (enter the letter e)");
		System.out.println("- quit enter the letter q)");
	
		while(choice != 'q') {

			System.out.println("Enter choice: ");
			choice = input.nextLine().charAt(0);

			switch(choice) {
				
				case 'a':
					System.out.print("Enter number to be added: ");
					temp = input.nextInt();
					input.nextLine();
					list.add(temp);
					System.out.println(temp + " is added");
					break;
				case 'd':
					System.out.print("Enter number to be deleted: ");
					temp = input.nextInt();
					input.nextLine();
					list.delete(temp);
					System.out.println(temp + " is deleted");
					break;
				case 'x':
					try {
						System.out.println("Max is " + list.max());
					}
					catch(NullPointerException e) {
						System.out.println("List is empty!");
					}
					break;
				case 'm':
					try {
						System.out.println("Min is " + list.min());
					}
					catch(NullPointerException e) {
						System.out.println("List is empty!");
					}
					break;
				case 'p':
					list.print();
					System.out.println();
					break;
				case 'e':
					if(list.isEmpty())
						System.out.println("List is empty!");
					else
						System.out.println("List is not empty!");
					break;
				case 'q':
					System.out.println("Quitting");
					break;
				default:
					System.out.println("Invalid Choice!");
					break;

			}
		}
	}

}
