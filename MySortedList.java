public class MySortedList {

	private class Node {

		int element;
		Node next;
		
		public Node(int data) {
			element = data;
		}
	}

	private Node head;

	public MySortedList() {

		head = null;

	}

	public void add(int item) {

		Node temp;

		if(head == null) {

			head = new Node(item);
		
		}
		else {
			
			temp = new Node(item);
			Node current;

			if(item <= head.element) {
				temp.next = head;
				head = temp;
			}
			else {
				current = head;
				while(current.next != null && current.next.element < item)
					current = current.next;
				temp.next = current.next;
				current.next = temp;
			}
			
		}

	}
	
	public void delete(int item) {
		
		Node current;

		if(head != null) {

			if(item == head.element) 
				head = head.next;
			else {

				current = head;
				while(current.next != null && current.next.element < item)
					current = current.next;
				if(current.next != null)
				{
					if(current.next.element == item)
							current.next = current.next.next;
			        }
			
			}

		}

	}

	public int max() {
		
		Node current = head;

		while(current.next != null)
			current = current.next;
		return current.element;

	}

	public int min() {
	
		return head.element;

	}

	public void print() {
	
		Node current = head;

		while(current != null) {
			System.out.print(current.element + " ");
			current = current.next;
		}

	}
	
	public boolean isEmpty() {
		return head == null;
	}


}
